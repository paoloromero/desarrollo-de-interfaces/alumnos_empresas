<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property int|null $telefono
 * @property string|null $fecha_nacimiento
 *
 * @property Acuden[] $acudens
 * @property Empresas[] $cifEmpresas
 * @property Cursos[] $codigoCursos
 * @property Cursan[] $cursans
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['telefono'], 'integer'],
            [['fecha_nacimiento'], 'safe'],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 55],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'telefono' => 'Telefono',
            'fecha_nacimiento' => 'Fecha Nacimiento',
        ];
    }

    /**
     * Gets query for [[Acudens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcudens()
    {
        return $this->hasMany(Acuden::className(), ['dni_alumnos' => 'dni']);
    }

    /**
     * Gets query for [[CifEmpresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCifEmpresas()
    {
        return $this->hasMany(Empresas::className(), ['cif' => 'cif_empresa'])->viaTable('acuden', ['dni_alumnos' => 'dni']);
    }

    /**
     * Gets query for [[CodigoCursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCursos()
    {
        return $this->hasMany(Cursos::className(), ['codigo' => 'codigo_cursos'])->viaTable('cursan', ['dni_alumnos' => 'dni']);
    }

    /**
     * Gets query for [[Cursans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCursans()
    {
        return $this->hasMany(Cursan::className(), ['dni_alumnos' => 'dni']);
    }
}
