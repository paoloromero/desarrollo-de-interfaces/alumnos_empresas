<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresas".
 *
 * @property string $cif
 * @property string|null $nombre
 * @property string|null $direccion
 * @property int|null $telefono
 * @property string|null $tutor
 *
 * @property Acuden[] $acudens
 * @property Alumnos[] $dniAlumnos
 */
class Empresas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cif'], 'required'],
            [['telefono'], 'integer'],
            [['cif'], 'string', 'max' => 10],
            [['nombre'], 'string', 'max' => 50],
            [['direccion'], 'string', 'max' => 120],
            [['tutor'], 'string', 'max' => 60],
            [['cif'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cif' => 'Cif',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'tutor' => 'Tutor',
        ];
    }

    /**
     * Gets query for [[Acudens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcudens()
    {
        return $this->hasMany(Acuden::className(), ['cif_empresa' => 'cif']);
    }

    /**
     * Gets query for [[DniAlumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniAlumnos()
    {
        return $this->hasMany(Alumnos::className(), ['dni' => 'dni_alumnos'])->viaTable('acuden', ['cif_empresa' => 'cif']);
    }
}
