<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cursan".
 *
 * @property int $id_cursan
 * @property string|null $dni_alumnos
 * @property string|null $codigo_cursos
 * @property string|null $fecha_inicio
 * @property string|null $fecha_fin
 *
 * @property Cursos $codigoCursos
 * @property Alumnos $dniAlumnos
 */
class Cursan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cursan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['dni_alumnos'], 'string', 'max' => 9],
            [['codigo_cursos'], 'string', 'max' => 6],
            [['dni_alumnos', 'codigo_cursos'], 'unique', 'targetAttribute' => ['dni_alumnos', 'codigo_cursos']],
            [['dni_alumnos'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['dni_alumnos' => 'dni']],
            [['codigo_cursos'], 'exist', 'skipOnError' => true, 'targetClass' => Cursos::className(), 'targetAttribute' => ['codigo_cursos' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cursan' => 'Id Cursan',
            'dni_alumnos' => 'Dni Alumnos',
            'codigo_cursos' => 'Codigo Cursos',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
        ];
    }

    /**
     * Gets query for [[CodigoCursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCursos()
    {
        return $this->hasOne(Cursos::className(), ['codigo' => 'codigo_cursos']);
    }

    /**
     * Gets query for [[DniAlumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniAlumnos()
    {
        return $this->hasOne(Alumnos::className(), ['dni' => 'dni_alumnos']);
    }
}
