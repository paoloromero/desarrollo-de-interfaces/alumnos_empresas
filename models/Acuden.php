<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "acuden".
 *
 * @property int $id_acuden
 * @property string|null $cif_empresa
 * @property string|null $dni_alumnos
 * @property string|null $fecha_inicio
 * @property string|null $fecha_final
 *
 * @property Empresas $cifEmpresa
 * @property Alumnos $dniAlumnos
 */
class Acuden extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'acuden';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_final'], 'safe'],
            [['cif_empresa'], 'string', 'max' => 10],
            [['dni_alumnos'], 'string', 'max' => 9],
            [['cif_empresa', 'dni_alumnos'], 'unique', 'targetAttribute' => ['cif_empresa', 'dni_alumnos']],
            [['dni_alumnos'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['dni_alumnos' => 'dni']],
            [['cif_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresas::className(), 'targetAttribute' => ['cif_empresa' => 'cif']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_acuden' => 'Id Acuden',
            'cif_empresa' => 'Cif Empresa',
            'dni_alumnos' => 'Dni Alumnos',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_final' => 'Fecha Final',
        ];
    }

    /**
     * Gets query for [[CifEmpresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCifEmpresa()
    {
        return $this->hasOne(Empresas::className(), ['cif' => 'cif_empresa']);
    }

    /**
     * Gets query for [[DniAlumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniAlumnos()
    {
        return $this->hasOne(Alumnos::className(), ['dni' => 'dni_alumnos']);
    }
}
