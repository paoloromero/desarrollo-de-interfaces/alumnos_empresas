<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cursos".
 *
 * @property string $codigo
 * @property string|null $nombre
 * @property string|null $duracion
 *
 * @property Cursan[] $cursans
 * @property Alumnos[] $dniAlumnos
 */
class Cursos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cursos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['duracion'], 'safe'],
            [['codigo'], 'string', 'max' => 6],
            [['nombre'], 'string', 'max' => 45],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'duracion' => 'Duracion',
        ];
    }

    /**
     * Gets query for [[Cursans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCursans()
    {
        return $this->hasMany(Cursan::className(), ['codigo_cursos' => 'codigo']);
    }

    /**
     * Gets query for [[DniAlumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniAlumnos()
    {
        return $this->hasMany(Alumnos::className(), ['dni' => 'dni_alumnos'])->viaTable('cursan', ['codigo_cursos' => 'codigo']);
    }
}
